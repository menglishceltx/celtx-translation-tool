package com.celtx;

import com.celtx.security.SecurityUtils;
import com.celtx.types.Xliff;
import com.celtx.utilities.XliffUtility;
import com.google.template.soy.SoyMsgExtractor;
import org.kohsuke.args4j.CmdLineException;
import org.kohsuke.args4j.CmdLineParser;
import org.kohsuke.args4j.Option;
import org.kohsuke.args4j.spi.StringArrayOptionHandler;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@SuppressWarnings({"FieldMayBeFinal", "DefaultAnnotationParam"})
public class Main {

    @Option(
            name = "--help",
            aliases = {"-help", "--h", "-h", "help"},
            required = false,
            usage = "Prints out all valid arguments and a description of each"
    )
    private boolean printHelp = false;

    @Option(
            name = "--uxPath",
            usage = "The full path to the current ux directory. This will be used to find and parse the .soy files.",
            required = true
    )
    private File uxDirectory;

    @Option(
            name = "--blankTranslationOutput",
            required = false,
            usage = "The path in which to output all the blank translation files, these are the files with all the " +
                    "target translations removed. Only required if you want the blank files to be saved."
    )
    private File blankTranslationOutput;

    @Option(
            name = "--xyzTranslationOutput",
            required = false,
            usage = "The path in which to output all the xyz xliff files, these are the files with all the " +
                    "target translations removed and replaced with XXYYZZ. If this is not specified the xliff files are " +
                    "placed in the starting translations folder." +
                    "This argument is required to do an xyz build."
    )
    private File xyzTranslationOutput;

    @Option(
            name = "--xyzBuild",
            required = false,
            usage = "A flag which indicates that an xyz build of the translations should be done. This is usually used " +
                    "for testing purposes" +
                    "This argument is required to do an xyz build."
    )
    private boolean doXyzBuild = false;

    @Option(
            name = "--newTranslationBuild",
            required = false,
            usage = "A flag which indicates that a set of files will be generated, where each file only contains the " +
                    "strings which need to be translated and we do not have translations for. These are the files that " +
                    "can be sent to the translation provider." +
                    "This argument is required to do an new translation build."
    )
    private boolean doNewTranslationBuild = false;

    @Option(
            name = "--newTranslationOutput",
            required = false,
            usage = "The path in which to output all the new xliff files, where each file only contains the " +
                    "strings which need to be translated and we do not have translations for. These are the files that " +
                    "can be sent to the translation provider." +
                    "This argument is required to do an new translation build."
    )
    private File newTranslationOutput;

    @Option(
            name = "--currentTranslationDirectory",
            required = false,
            usage = "The path in which the current translations being used in UX are stored." +
                    "This argument is required to do an new translation build."
    )
    private File currentTranslationDirectory;

    @Option(
            name = "--outputLanguages",
            required = false,
            handler = StringArrayOptionHandler.class,
            usage = "The languages for which files should be generated. The default is: de, en-US, es-ES, fr and pt-BR. " +
                    "Arguments should be a space separated list, i.e. \"de fr\""
    )
    private List<String[]> outputLanguages = new ArrayList();

    public static void main(String... args) throws IOException {
        new Main().handleArguments(args);
    }

    public void handleArguments(String... arguments) throws IOException {
        performArgumentVerification(arguments);

        if (outputLanguages.size() == 0) {
            String[] languages = {"de", "en-US", "es-ES", "fr", "pt-BR"};
            outputLanguages.add(languages);
        }

        List<Path> soyFiles = getSoyFiles(uxDirectory);
        Map<String, Path> xlfFiles = new HashMap<>();
        for (String language : outputLanguages.get(0)) {
            Path soyXlf = getXlfFromSoy(soyFiles, language, Files.createTempFile("", language + ".xlf"));
            //This is a temporary file, we don't want to leave a bunch around so delete it when the JVM dies
            soyXlf.toFile().deleteOnExit();
            xlfFiles.put(language, soyXlf);
        }

        Map<String, Xliff> blankXliffMaps = new HashMap<>();

        for (String language : xlfFiles.keySet()) {
            Xliff xliff = XliffUtility.parseXlfFile(xlfFiles.get(language));
            XliffUtility.fixInvalidXml(xliff);
            blankXliffMaps.put(language, xliff);
        }

        if (blankTranslationOutput != null && (!blankTranslationOutput.exists() || blankTranslationOutput.mkdirs())) {
            for (String language : xlfFiles.keySet()) {
                Path output = blankTranslationOutput.toPath().resolve(language + ".xlf");
                File outputFile = output.toFile();
                if ((outputFile.exists() && !outputFile.delete()) || (!outputFile.exists() && !outputFile.getParentFile().mkdirs())) {
                    continue;
                }
                System.out.println("Output: " + output.toString());
                System.out.println("Input: " + xlfFiles.get(language).toString());

                Files.copy(xlfFiles.get(language), output);
            }
        }

        if (doXyzBuild) {
            if (!xyzTranslationOutput.exists() && !xyzTranslationOutput.mkdirs()) {
                return;
            }
            for (String language : blankXliffMaps.keySet()) {
                XliffUtility.setAllTranslationTargetsToUnique(blankXliffMaps.get(language), "XXYYZZ");
                XliffUtility.fixInvalidXml(blankXliffMaps.get(language));

                Path output = xyzTranslationOutput.toPath().resolve(language + ".xlf");
                File outputFile = output.toFile();
                if (outputFile.exists() && !outputFile.delete()) {
                    continue;
                }

                XliffUtility.writeXliffToFile(
                    blankXliffMaps.get(language),
                    xyzTranslationOutput.toPath().resolve(language + ".xlf")
                );
            }
        }

        if (doNewTranslationBuild) {
            if (doXyzBuild) {
                blankXliffMaps.values().forEach(xliff -> XliffUtility.setAllTranslationTargetsTo(xliff, ""));
            }

            if (!newTranslationOutput.exists() && !newTranslationOutput.mkdirs()) {
                return;
            }
            Map<String, Xliff> oldTranslationXlfs = new HashMap<>();

            try (Stream<Path> walkStream = Files.walk(currentTranslationDirectory.toPath(), 1)) {
                walkStream
                    .filter(path -> path.getFileName().toString().endsWith(".xlf"))
                    .forEach(path -> {
                        for (String language : outputLanguages.get(0)) {
                            if (path.getFileName().toString().endsWith(language + ".xlf")) {
                                try {
                                    oldTranslationXlfs.put(language, XliffUtility.parseXlfFile(path));
                                    return;
                                } catch (IOException exception) {
                                    exception.printStackTrace();
                                }
                            }
                        }
                    });
            } catch (IOException exception) {
                exception.printStackTrace();
            }

            for (String language : blankXliffMaps.keySet()) {
                if (oldTranslationXlfs.containsKey(language)) {
                    XliffUtility.removeNonUniqueTranslations(blankXliffMaps.get(language), oldTranslationXlfs.get(language));
                }

                Path output = newTranslationOutput.toPath().resolve(language + ".xlf");
                File outputFile = output.toFile();
                if (outputFile.exists() && !outputFile.delete()) {
                    continue;
                }

                XliffUtility.writeXliffToFile(
                    blankXliffMaps.get(language),
                    newTranslationOutput.toPath().resolve(language + ".xlf")
                );
            }
        }
    }

    private void performArgumentVerification(String... arguments) {
        final CmdLineParser parser = new CmdLineParser(this);
        if (arguments.length < 1) {
            printHelp = true;
        }

        try {
            parser.parseArgument(arguments);
        } catch (CmdLineException clEx) {
            System.out.println("ERROR: Unable to parse command-line options: " + clEx);
        } finally {
            if (printHelp) {
                parser.printUsage(System.out);
                System.exit(-1);
            }
        }

        if (doXyzBuild && xyzTranslationOutput == null) {
            System.out.println("Error: A xyz build was specified but no output for the xyz build was specified. " +
                    "Please try again, supplying both xyz required arguments.");
            System.out.println("See `--help` for more details");
            System.exit(-1);
        } else if (!doXyzBuild && xyzTranslationOutput != null) {
            System.out.println("Error: A xyz build output specified but the xyz build flag was not specified. " +
                    "Please try again, supplying both xyz required arguments.");
            System.out.println("See `--help` for more details");
            System.exit(-1);
        }

        if (doNewTranslationBuild && newTranslationOutput == null) {
            System.out.println("Error: A new translation build was specified but no output for the new translation files" +
                    "were specified. Please try again, supplying both new translation required arguments.");
            System.out.println("See `--help` for more details");
            System.exit(-1);
        } else if (!doNewTranslationBuild && newTranslationOutput != null) {
            System.out.println("Error: A new translation output was specified but the new translation build flag was " +
                    "not. Please try again, supplying both new translation required arguments.");
            System.out.println("See `--help` for more details");
            System.exit(-1);
        }
    }

    /**
     * Searches through the specified ux directory to find all .soy files. Then returns a list with the paths of those
     * files.
     *
     * Note: Any .soy files found to be in a subdirectory of 'cxsrv' are excluded, this was legacy behaviour copied
     * from the old bash script.
     *
     * @param uxDirectory {@link Path} to the ux directory to search for .soy files in
     * @return {@link List} of {@link Path}s where each list item is a path to a .soy file
     */
    private List<Path> getSoyFiles(File uxDirectory) {
        Path uxPath = uxDirectory.toPath();
        final int uxNameIndex = uxPath.getNameCount() - 1; //We want the name of our ux folder so we can check sub folders.
        try (Stream<Path> walkStream = Files.walk(uxDirectory.toPath())) {
            return walkStream
                    .filter(path -> path.getFileName().toString().endsWith(".soy"))
                    .filter(path -> !path
                            .subpath(uxNameIndex, path.getNameCount())
                            .toString()
                            .contains(File.separator + "cxsrv" + File.separator)
                    )
                    .collect(Collectors.toList());
        } catch (IOException exception) {
            exception.printStackTrace();
        }

        return new ArrayList<>();
    }

    /**
     * Takes a list of soy files which contain text to be translated, pulls out the text and creates a single xlf file.
     *
     * @param soyFiles {@link List} of {@link Path}s which represent the soy files to create the xlf from
     * @param languageName The name of the language this xlf will represent
     * @param outputFile {@link Path} indicating where the created xlf should live
     * @return {@link Path} object representing the newly created xlf file.
     */
    private Path getXlfFromSoy(List<Path> soyFiles, String languageName, Path outputFile) throws IOException {
        //Given we call the main method, the arguments must be identical to what would be entered via the command line
        String[] arguments = {
                "--targetLocaleString", languageName,
                "--outputFile", outputFile.toString(),
                "--srcs", soyFiles.stream().map(Path::toString).collect(Collectors.joining(",")),
        };

        //The SoyMsgExtractor calls System.exit after it runs. We don't want that behavior so we abuse the Java
        //Security Manager to allow us to prevent those calls from doing anything. Which allows our program to
        //continue to run. We swap out the security manager for our custom one, run the extractor and then put the old
        //manager back
        SecurityManager securityManager = System.getSecurityManager();
        System.setSecurityManager(new SecurityUtils.SystemExitManager());
        try {
            SoyMsgExtractor.main(arguments);
        }
        catch (SecurityUtils.ExitTrappedException ignored) {}
        System.setSecurityManager(securityManager);

        return outputFile;
    }
}
