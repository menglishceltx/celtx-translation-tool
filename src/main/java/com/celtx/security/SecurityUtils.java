package com.celtx.security;

import java.security.Permission;

public class SecurityUtils {

    public static class SystemExitManager extends SecurityManager {
        public void checkPermission(Permission permission) {
            if (permission.getName().contains("exitVM")) {
                throw new ExitTrappedException();
            }
        }
    }


    public static class ExitTrappedException extends SecurityException {}
}
