package com.celtx.utilities;

import com.celtx.types.TranslationUnit;
import com.celtx.types.Xliff;
import com.ctc.wstx.api.WstxOutputProperties;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.dataformat.xml.XmlMapper;
import com.fasterxml.jackson.dataformat.xml.ser.ToXmlGenerator;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.*;

@SuppressWarnings({"unused", "RedundantSuppression"})
public class XliffUtility {

    /**
     * Produces a parsed {@link Xliff} object from an xlf file
     *
     * @param xlfPath The {@link Path} of the file to be parsed
     * @return The parsed {@link Xliff} object
     */
    public static Xliff parseXlfFile(Path xlfPath) throws IOException {
        return new XmlMapper().readValue(new String(Files.readAllBytes(xlfPath)), Xliff.class);
    }

    /**
     * Parses an {@link Xliff} object and removes any {@link TranslationUnit} who's target value has already been seen.
     * The end result is an Xliff object with a list of TranslationUnits which all have unique translations.
     *
     * @param xliff The {@link Xliff} object to remove duplicates from
     */
    public static void removeDuplicateTranslationUnits(Xliff xliff) {
        List<TranslationUnit> translationUnits = xliff.getFile().getTranslationUnits();
        Set<String> translationTargets = new HashSet<>();

        translationUnits.removeIf(translationUnit -> !translationTargets.add(translationUnit.getTarget()));
        xliff.getFile().setTranslationUnits(translationUnits);
    }

    /**
     * Iterates through the source and target values of each {@link TranslationUnit} in the {@link Xliff} object and
     * attempts to correct any invalid xml. As if the xml is invalid the parsers might have issues
     *
     * @param xliff The {@link Xliff} object who's target and source values we want to fix.
     */
    public static void fixInvalidXml(Xliff xliff) {
        for (TranslationUnit translationUnit : xliff.getFile().getTranslationUnits()) {
            translationUnit.setTarget(fixAmpersands(translationUnit.getTarget()));
            translationUnit.setSource(fixAmpersands(translationUnit.getSource()));
        }
    }

    /**
     * Writes the given {@link Xliff} object out to a file as an xml document
     *
     * @param xliff The {@link Xliff} file to write out
     * @param outputPath The {@link Path} where the xliff should be written to.
     */
    public static void writeXliffToFile(Xliff xliff, Path outputPath) throws IOException {
        File outputFile = outputPath.toFile();
        if (outputFile.isDirectory() || !outputFile.createNewFile() || !outputFile.canWrite()) {
            return;
        }

        XmlMapper mapper = new XmlMapper();
        mapper.configure( ToXmlGenerator.Feature.WRITE_XML_DECLARATION, true );
        mapper.enable(SerializationFeature.INDENT_OUTPUT);
        mapper.getFactory().getXMLOutputFactory().setProperty(WstxOutputProperties.P_USE_DOUBLE_QUOTES_IN_XML_DECL, true);
        mapper.writeValue(outputFile, xliff);
    }

    /**
     * Iterates through the {@link TranslationUnit}s in the {@link Xliff} and replaces the target in each unit with the
     * specified string
     *
     * @param xliff The {@link Xliff} file who's targets should be modified
     * @param translationTarget The new String for each target
     */
    public static void setAllTranslationTargetsTo(Xliff xliff, String translationTarget) {
        for (TranslationUnit translationUnit : xliff.getFile().getTranslationUnits()) {
            translationUnit.setTarget(translationTarget);
        }
    }

    /**
     * Iterates through the {@link TranslationUnit}s in the {@link Xliff} and replaces the target in each unit with the
     * specified string
     *
     * @param xliff The {@link Xliff} file who's targets should be modified
     * @param translationTargetPrefix The new prefix String for each target
     */
    public static void setAllTranslationTargetsToUnique(Xliff xliff, String translationTargetPrefix) {
        List<TranslationUnit> units = xliff.getFile().getTranslationUnits();
        for (int i = 0; i < xliff.getFile().getTranslationUnits().size(); i++) {
            units.get(i).setTarget(translationTargetPrefix + "_" + i);
        }
    }

    /**
     * Compares two {@link Xliff} objects and removes all {@link TranslationUnit}s from the newXliff if they are present
     * in the oldXliff. Modifications are done inline to the passed values.
     *
     * @param newXliff The {@link Xliff} file who's translation units may be removed
     * @param oldXliff The {@link Xliff} file to compare against
     */
    public static void removeNonUniqueTranslations(Xliff newXliff, Xliff oldXliff) {
        Map<String, TranslationUnit> newSourceMap = getUniqueTranslationMap(newXliff);
        Map<String, TranslationUnit> oldSourceMap = getUniqueTranslationMap(oldXliff);

        for (String source : oldSourceMap.keySet()) {
            if (newSourceMap.containsKey(source)) {
                newXliff.getFile().getTranslationUnits().remove(newSourceMap.get(source));
            }
        }
    }

    /**
     * Generates a map of all the source translation strings and their corresponding translation units
     *
     * @param xliff The {@link Xliff} object to generate the map from
     * @return A {@link Map<String, TranslationUnit>} keyed on the source text with values of the translation unit the
     *         source text resides in
     */
    private static Map<String, TranslationUnit> getUniqueTranslationMap(Xliff xliff) {
        Map<String, TranslationUnit> translationMap = new HashMap<>();
        for (TranslationUnit translationUnit : xliff.getFile().getTranslationUnits()) {
            translationMap.putIfAbsent(translationUnit.getSource(), translationUnit);
        }

        return translationMap;
    }

    /**
     * Attempts to fix any incorrect ampersands in the XML text by attempting to escape the ampersands correctly.
     *
     * @param incomingString The string who's text we want to fix
     * @return The fixed string.
     */
    private static String fixAmpersands(String incomingString) {
        return incomingString.replaceAll("(&([^a][^m][^p].*;))|(&( ))", "&amp;$2$4");
    }
}
