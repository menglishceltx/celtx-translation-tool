package com.celtx.types;

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlRootElement;

@SuppressWarnings({"DefaultAnnotationParam", "unused", "RedundantSuppression"})
@JacksonXmlRootElement(localName = "xliff")
public class Xliff {
    @JacksonXmlProperty(localName = "file", isAttribute = false)
    private XliffFile file;

    @JacksonXmlProperty(localName = "version", isAttribute = true)
    private String version;

    @JacksonXmlProperty(localName = "xmlns", isAttribute = true)
    private String namespace = "urn:oasis:names:tc:xliff:document:1.2";

    public XliffFile getFile() {
        return file;
    }

    public void setFile(XliffFile file) {
        this.file = file;
    }

    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }

    public String getNamespace() {
        return namespace;
    }

    public void setNamespace(String namespace) {
        this.namespace = namespace;
    }
}
