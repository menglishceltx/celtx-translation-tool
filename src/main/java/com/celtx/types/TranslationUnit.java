package com.celtx.types;

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlRootElement;

@SuppressWarnings({"DefaultAnnotationParam", "unused", "RedundantSuppression"})
public class TranslationUnit {

    @JacksonXmlProperty(localName = "source", isAttribute = false)
    private String source;

    @JacksonXmlProperty(localName = "target", isAttribute = false)
    private String target;

    @JacksonXmlProperty(localName = "note", isAttribute = false)
    private TranslationNote translationNote;

    @JacksonXmlProperty(localName = "datatype", isAttribute = true)
    private String dataType;

    @JacksonXmlProperty(localName = "id", isAttribute = true)
    private String id;

    @JacksonXmlProperty(localName = "translate", isAttribute = true)
    private String translate;

    public String getDataType() {
        return dataType;
    }

    public void setDataType(String dataType) {
        this.dataType = dataType;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTarget() {
        return target;
    }

    public void setTarget(String target) {
        this.target = target;
    }

    public TranslationNote getTranslationNote() {
        return translationNote;
    }

    public void setTranslationNote(TranslationNote translationNote) {
        this.translationNote = translationNote;
    }

    public String getSource() {
        return source;
    }

    public void setSource(String source) {
        this.source = source;
    }

    public String getTranslate() {
        return translate;
    }

    public void setTranslate(String translate) {
        this.translate = translate;
    }
}
