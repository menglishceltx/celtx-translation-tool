package com.celtx.types;

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlElementWrapper;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;

import java.util.List;

@SuppressWarnings({"DefaultAnnotationParam", "unused", "RedundantSuppression"})
public class XliffFile {
    @JacksonXmlElementWrapper(localName = "body")
    @JacksonXmlProperty(localName = "trans-unit", isAttribute = false)
    private List<TranslationUnit> translationUnits;

    @JacksonXmlProperty(localName = "datatype", isAttribute = true)
    private String dataType;

    @JacksonXmlProperty(localName = "source-language", isAttribute = true)
    private String sourceLanguage;

    @JacksonXmlProperty(localName = "space", isAttribute = true)
    private String xmlSpace;

    @JacksonXmlProperty(localName = "original", isAttribute = true)
    private String original;

    @JacksonXmlProperty(localName = "target-language", isAttribute = true)
    private String targetLanguage;

    public List<TranslationUnit> getTranslationUnits() {
        return translationUnits;
    }

    public void setTranslationUnits(List<TranslationUnit> translationUnits) {
        this.translationUnits = translationUnits;
    }

    public String getDataType() {
        return dataType;
    }

    public void setDataType(String dataType) {
        this.dataType = dataType;
    }

    public String getSourceLanguage() {
        return sourceLanguage;
    }

    public void setSourceLanguage(String sourceLanguage) {
        this.sourceLanguage = sourceLanguage;
    }

    public String getXmlSpace() {
        return xmlSpace;
    }

    public void setXmlSpace(String xmlSpace) {
        this.xmlSpace = xmlSpace;
    }

    public String getOriginal() {
        return original;
    }

    public void setOriginal(String original) {
        this.original = original;
    }

    public String getTargetLanguage() {
        return targetLanguage;
    }

    public void setTargetLanguage(String targetLanguage) {
        this.targetLanguage = targetLanguage;
    }
}
