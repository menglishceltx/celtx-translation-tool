package com.celtx.types;

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlText;

@SuppressWarnings({"DefaultAnnotationParam", "unused", "RedundantSuppression"})
public class TranslationNote {

    @JacksonXmlProperty(localName = "priority", isAttribute = true)
    private int priority;
    @JacksonXmlProperty(localName = "from", isAttribute = true)
    private String from;
    @JacksonXmlText
    private String value;

    public int getPriority() {
        return priority;
    }

    public void setPriority(int priority) {
        this.priority = priority;
    }

    public String getFrom() {
        return from;
    }

    public void setFrom(String from) {
        this.from = from;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }
}
