# Celtx Translation Tool

## About

A Java based utility to make it easier to create the `.xlf` files used by Celtx for translations.

## Usage

The tool has three main pieces of functionality which are:

- Creation of blank `.xlf` files, which have all the strings _to_ be translated but no translations
- Creation of translation ready `.xlf` files, which have all the strings _to_ be translated which are not already translated.
  - These are the files which are to be sent to the translation company (i.e. Lionbridge)
- Creation of `XYZ` `.xlf` files, which have all the strings to be translated and their translations set to a string like `XXYYZZ`.
  - This is used to allow a developer to navigate through UX and find strings which are not marked for translation. As any string not translated to `XXYYZZ` has not been marked for translation and will need to

The functions are all controlled by command line flags, which are outlined below (note the order in which the flags specified does not matter):

- `--help`
    - Prints out all valid arguments and a description of each
- `--uxPath`
    - The full path to the current ux directory. This will be used to find and parse the .soy files. This argument is required for all commands
- `--blankTranslationOutput`
    - The path in which to output all the blank translation files, these are the files with all the target translations removed. Only required if you want the blank files to be saved.
- `--xyzBuild`
    - A flag which indicates that an xyz build of the translations should be done. This is usually used for verifying strings are marked for translation. This argument is required to do an xyz build.
- `--xyzTranslationOutput`
    - The path in which to output all the xyz xliff files, these are the files with all the target translations removed and replaced with XXYYZZ. If this is not specified the xliff files are placed in the starting translations folder. This argument is required to do an xyz build.
- `--newTranslationBuild`
    - A flag which indicates that a set of files will be generated, where each file only contains the strings which need to be translated and we do not have translations for. These are the files that can be sent to the translation provider. This argument is required to do an new translation build.
- `--newTranslationOutput`
    - The path in which to output all the new xliff files, where each file only contains the strings which need to be translated and we do not have translations for. These are the files that can be sent to the translation provider. This argument is required to do an new translation build.
- `--currentTranslationDirectory`
    - The path in which the current translations being used in UX are stored. This argument is required to do an new translation build.
- `--outputLanguages`
    - The languages for which files should be generated. If the flag is not specified the list defaults to: de, en-US, es-ES, fr and pt-BR. Arguments should be a comma separated list, i.e. `de,fr`

### Example commands

- **Generating Blank Translation Files**
    - 
    ```shell
    java -jar celtx-translation-tool-*.jar \
        --uxPath "/Users/CeltxDev/celtx/ux/" \
        --blankTranslationOutput "/Users/CeltxDev/celtx/ux/blank-translations"
    ```

- **Generating XYZ Translation Files**
    - 
    ```shell
    java -jar celtx-translation-tool-*.jar \
        --uxPath "/Users/CeltxDev/celtx/ux/" \
        --xyzBuild \
        --xyzTranslationOutput "/Users/CeltxDev/celtx/ux/xyz-translations"
    ```

- **Generating New Translation Files**
    - 
    ```shell
    java -jar celtx-translation-tool-*.jar \
        --uxPath "/Users/CeltxDev/celtx/ux/" \
        --newTranslationBuild \
        --newTranslationOutput "/Users/CeltxDev/celtx/ux/new-translations" \
        --currentTranslationDirectory "/Users/CeltxDev/celtx/ux/translations"
    ```

- **Running all commands at once**
    - 
    ```shell
    java -jar celtx-translation-tool-*.jar \
        --uxPath "/Users/CeltxDev/celtx/ux/" \
        --blankTranslationOutput "/Users/CeltxDev/celtx/blank-translations" \
        --newTranslationBuild \
        --newTranslationOutput "/Users/CeltxDev/celtx/ux/new-translations" \
        --xyzBuild \
        --xyzTranslationOutput "/Users/CeltxDev/celtx/xyz-translations" \
        --currentTranslationDirectory "/Users/CeltxDev/celtx/ux/translations" \
        --outputLanguages "de fr"
    ```

## Build

A release build should be done with the `shadowJar` gradle task, as this will build a fat jar (a jar file with all dependencies included) allowing for easy usage by developers.

An example build command would be: `./gradlew shadowJar `
